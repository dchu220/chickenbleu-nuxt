export default {
  menu: {
    title: 'Pre-Opening Menu',
    courses: [
      {
        title: 'Main',
        items: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Seoul Chicken',
            subtitle: 'Korean chili sauce, kimchi, and grilled onion'
          },
          {
            title: 'All American',
            subtitle: 'Lettuce, tomato, pickles and ranch dressing'
          },
          {
            title: "Fidel's Chicken",
            subtitle:
              'Stack of ham, horseradish dijon mustard, mayonnaise, dill pickles, and provolone cheese'
          },
          {
            title: "C'est la vie",
            subtitle: 'Seared foie gras, balsamic and mayonnaise'
          },
          {
            title: 'The Italian Job',
            subtitle: 'Provolone cheese, prosciutto, pesto mayonnaise'
          }
        ]
      },
      {
        title: 'Side',
        items: [
          {
            title: 'French Connection Fries',
            subtitle: 'Freshly cut french fries seared in foie gras oil'
          }
        ]
      },
      {
        title: 'Drinks',
        items: [
          {
            title: 'Soda'
          },
          {
            title: 'Beer'
          }
        ]
      }
    ],
    location: {
      name: 'The Oasis Taver',
      address: 'english address',
      url: 'http://espn.com'
    }
  },
  menus: [
    {
      title: 'Caesar the Great',
      subtitle: 'A sous vide chicken breast and stuff.',
      items: [
        {
          title: 'Caesar the Great',
          subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
        },
        {
          title: 'Fries',
          subtitle: 'fries description'
        },
        {
          title: 'Drink',
          subtitle: 'drink description'
        }
      ],
      plates_available: 15,
      plates_sold: 0,
      price: '300 NTD',
      date: '04/24',
      time: '7:30 PM - 8:30PM',
      location: {
        name: 'The Oasis Taver',
        address: 'english address',
        url: 'http://espn.com'
      }
    }
  ]
}
