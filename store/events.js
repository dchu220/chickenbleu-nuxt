export const state = () => ({
  items: [
    {
      id: 1,
      en: {
        title: 'Caesar the Great ENGLISH',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      tw: {
        title: 'Caesar the Great CHINESE',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      image_url: '/sandwich.jpg',
      plates_available: 15,
      plates_sold: 0,
      price: '300 NTD',
      date: '04/24',
      time: '19:30 - 20:30'
    },
    {
      id: 2,
      en: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      tw: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      image_url: '/sandwich.jpg',
      plates_available: 15,
      plates_sold: 0,
      price: '300 NTD',
      date: '04/24',
      time: '19:30 - 20:30'
    },
    {
      id: 3,
      en: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      tw: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      image_url: '/sandwich.jpg',
      plates_available: 15,
      plates_sold: 0,
      price: '300 NTD',
      date: '04/24',
      time: '19:30 - 20:30'
    },
    {
      id: 4,
      en: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      tw: {
        title: 'Caesar the Great',
        subtitle: 'A sous vide chicken breast and stuff.',
        menu: [
          {
            title: 'Caesar the Great',
            subtitle: 'Caesar dressing, romain lettuce, and bacon crisps'
          },
          {
            title: 'Fries',
            subtitle: 'fries description'
          },
          {
            title: 'Drink',
            subtitle: 'drink description'
          }
        ],
        location: {
          name: 'The Oasis Taver',
          address: 'english address',
          url: 'http://espn.com'
        }
      },
      image_url: '/sandwich.jpg',
      plates_available: 15,
      plates_sold: 0,
      price: '300 NTD',
      date: '04/24',
      time: '19:30 - 20:30'
    }
  ]
})

export const actions = {}

// import { set } from '~/lib/store'
// export const mutations = {
//   set
// }
