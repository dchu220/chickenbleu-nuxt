const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat|Roboto+Slab'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: ['~/assets/css/custom.css'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        locales: [
          {
            name: 'English',
            code: 'en',
            iso: 'en-US',
            file: 'en-US.js'
          },
          {
            name: 'Chinese - Traditional',
            code: 'tw',
            iso: 'zh-TW',
            file: 'zh-TW.js'
          }
        ],
        langDir: 'language/',
        lazy: true,
        defaultLocale: 'tw',
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'i18n_redirected',
          alwaysRedirect: false
        }
      }
    ],
    [
      'nuxt-fontawesome',
      {
        component: 'fa',
        imports: [
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: [
              'faMapMarkerAlt',
              'faArrowCircleRight',
              'faBars',
              'faBuilding',
              'faSearch',
              'faVideo',
              'faCheck',
              'faInfo',
              'faCalendarAlt',
              'faEnvelope',
              'faChevronCircleRight',
              'faTimes',
              'faSync',
              'faExclamation',
              'faRss',
              'faChevronUp',
              'faChevronDown',
              'faLink',
              'faImage',
              'faCamera',
              'faSearchLocation',
              'faHeart',
              'faAngleLeft',
              'faEye',
              'faPlus'
            ]
          },
          {
            set: '@fortawesome/free-regular-svg-icons',
            icons: [
              'faCommentAlt',
              'faUserCircle',
              'faBell',
              'faImage',
              'faComment',
              'faHeart'
            ]
          }
        ]
      }
    ]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
